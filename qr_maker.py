import qrcode
import qrcode.image.svg

def main():
    # # define a method to choose which factory metho to use
    # # possible values 'basic' 'fragment' 'path'
    # method = "basic"

    # data = "Some text that you want to store in the qrcode"

    # if method == 'basic':
    #     # Simple factory, just a set of rects.
    #     factory = qrcode.image.svg.SvgImage
    # elif method == 'fragment':
    #     # Fragment factory (also just a set of rects)
    #     factory = qrcode.image.svg.SvgFragmentImage
    # elif method == 'path':
    #     # Combined path factory, fixes white space that may occur when zooming
    #     factory = qrcode.image.svg.SvgPathImage

    # # Set data to qrcode
    # img = qrcode.make(data, image_factory = factory)

    # # Save svg file somewhere
    # img.save("qrcode.svg")

    code = qrcode.make("The matrix is watching you, Rodas...", image_factory=qrcode.image.svg.SvgPathImage)
    code.save('code.svg')



if __name__ == '__main__':
    main()